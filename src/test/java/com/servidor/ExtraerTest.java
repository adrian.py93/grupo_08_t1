package com.servidor;


import com.api.Comunication;
import com.api.statics.Request;
import com.servidor.entity.Cuenta;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class ExtraerTest extends TestCase {
    public void testApp() throws IOException {
        Socket socket = null;
        try {
            int serverPort = 6880;
            String ip = "localhost";
            Comunication request = new Comunication(Request.extraer, 2L, 1000D);
            String data = request.toJson();
            socket = new Socket(ip, serverPort);
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());


            // inicio consulta BD
            //esto porcion de codigo uso para verificar el saldo antes del descuento

            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            long cuentaId = (long) 1;
            Cuenta cuenta = session.get(Cuenta.class, cuentaId);
            session.close();
            System.out.println("El saldo actual es: " + cuenta.getSaldo());
            //fin consulta BD

            //enviar datos al servidor
            outputStream.writeInt(data.length());
            outputStream.writeBytes(data);


            //leer respuesta del servidor
            int nb = inputStream.readInt();

            byte[] digit = new byte[nb];
            for (int i = 0; i < nb; i++)
                digit[i] = inputStream.readByte();

            String st = new String(digit);
            System.out.println(st);
//            Movimiento mo = Movimiento.fromJson(st);

//            System.out.println("El saldo luego de la extraccion es: " + mo.getCuenta().getSaldo());


        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if (socket != null)
                try {
                    socket.close();

                } catch (IOException e) {
                }
        }
    }
}



