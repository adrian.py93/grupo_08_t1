/**
 * @author SERGIO DAMIAN RIVEROS VAZQUEZ
 */
package com.servidor;

import com.api.Comunication;
import com.api.statics.Request;
import com.servidor.util.Poblacion;
import junit.framework.TestCase;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

//import com.servidor.util.Poblacion;

//se recibe los datos
public class depositarTest extends TestCase {
    public void testApp() throws IOException {
        Socket socket = null;

        try {
            Poblacion.poblacionDB();
            int serverPort = 6880;
            String ip = "localhost";
            Comunication request = new Comunication(Request.depositar, 1L, 1599502D);
            String data = request.toJson();
            socket = new Socket(ip, serverPort);
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

            //enviar datos al servidor
            outputStream.writeInt(data.length());
            outputStream.writeBytes(data);


            int nb = inputStream.readInt();
            byte[] digit = new byte[nb];
            for (int i = 0; i < nb; i++)
                digit[i] = inputStream.readByte();

            String st = new String(digit);
            System.out.println("Recibido: " + st);


        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if (socket != null)
                try {
                    socket.close();
                } catch (IOException e) {/*close failed*/}
        }
    }
}
