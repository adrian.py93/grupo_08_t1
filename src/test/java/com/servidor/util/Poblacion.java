package com.servidor.util;

import com.servidor.entity.Cuenta;
import com.servidor.entity.CuentaMovimiento;
import com.servidor.entity.Movimiento;
import com.servidor.entity.Usuario;
import com.servidor.statics.Estado;
import com.servidor.statics.Genero;
import com.servidor.statics.TipoMovimiento;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Calendar;

public class Poblacion {

    public static void poblacionDB() {
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 1993);
            cal.set(Calendar.MONTH, Calendar.NOVEMBER);
            cal.set(Calendar.DAY_OF_MONTH, 25);

            //crear objetos entidades tipo Usuario
            Usuario adrian = new Usuario(3582771, "adrian", "vera", Genero.M, cal.getTime(), Estado.Activo);

            cal.set(Calendar.YEAR, 1992);
            cal.set(Calendar.MONTH, Calendar.APRIL);
            cal.set(Calendar.DAY_OF_MONTH, 12);
            Usuario romina = new Usuario(1231241, "romina", "rotela", Genero.F, cal.getTime(), Estado.Activo);

            //se guarda en la base de datos
            session.save(adrian);
            session.save(romina);

            //crear objetos entidades tipo Cuentas
            Cuenta cuentaAdrian = new Cuenta(adrian, 5000000);
            Cuenta cuentaRomina = new Cuenta(romina, 10000000);
            session.save(cuentaAdrian);
            session.save(cuentaRomina);

            //crear objeto movimiento que represente las cuentas creadas
            Movimiento movimientoAdrian = new Movimiento(5000000);
            Movimiento movimientoRomina = new Movimiento(10000000);
            session.save(movimientoAdrian);
            session.save(movimientoRomina);

            CuentaMovimiento cuentaMovimientoAdrian = new CuentaMovimiento(cuentaAdrian, movimientoAdrian, TipoMovimiento.Capital);
            CuentaMovimiento cuentaMovimientoRomina = new CuentaMovimiento(cuentaRomina, movimientoRomina, TipoMovimiento.Capital);
            session.save(cuentaMovimientoAdrian);
            session.save(cuentaMovimientoRomina);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("La base de datos ya se encuentra poblada, o aún no ha sido creada");
        }
    }
}
