package com.servidor;

import com.servidor.entity.Usuario;
import com.servidor.statics.Estado;
import com.servidor.statics.Genero;
import junit.framework.TestCase;

import java.util.Calendar;

public class JsonUsuarioTest extends TestCase {
    public void testApp() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1993);
        cal.set(Calendar.MONTH, Calendar.NOVEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 25);
        Usuario adrian = new Usuario(3582771, "adrian", "vera", Genero.M, cal.getTime(), Estado.Activo);
        System.out.println(adrian.toJson());
    }
}
