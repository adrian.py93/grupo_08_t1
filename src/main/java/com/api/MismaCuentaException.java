package com.api;

public class MismaCuentaException extends Exception {
    public MismaCuentaException() {
        this("No se puede efectuar transacciones sobre la misma cuenta");
    }

    private MismaCuentaException(String message) {
        super(message);
    }
}
