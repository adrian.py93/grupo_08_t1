package com.api.statics;

public enum Error {
    SinError, cuentaSinSaldo, mismaCuenta, cuentaInvalida, errorDesconocido
}
