package com.servidor.entity;

import com.google.gson.Gson;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cuenta")
public class Cuenta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double saldo;
    private Usuario usuario;

    public Cuenta() {

    }

    public Cuenta(Usuario usuario, double saldo) {
        this.saldo = saldo;
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ci")
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return id + ' ' + usuario.toString();
    }

    public String toJson() {
        Gson gson = new Gson();
        return (gson.toJson(this));
    }
}
