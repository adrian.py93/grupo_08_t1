package com.servidor.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "registros")
public class Registros {
    @Column(name = "indata", length = 1024)
    private String indata;
    @Column(name = "outdata", length = 1024)
    private String outdata;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    public Registros() {

    }

    public Registros(String in, String out) {
        this.indata = in;
        this.outdata = out;
        this.time = Calendar.getInstance().getTime();
    }

    public Registros(String out) {
        this.indata = null;
        this.outdata = out;
        this.time = Calendar.getInstance().getTime();
    }

    public void save() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(this);
        session.getTransaction().commit();
        session.close();
    }
}
