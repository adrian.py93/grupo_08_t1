package com.servidor.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        try {
            int serverPort = 6880;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("server start listening... ... ...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                ServerThread c = new ServerThread(clientSocket);
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }

}