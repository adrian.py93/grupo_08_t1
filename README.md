**Trabajo practico sd banco**

P̶o̶r̶ ̶u̶n̶a̶ ̶m̶a̶l̶a̶ ̶l̶e̶c̶t̶u̶r̶a̶ ̶a̶c̶t̶u̶a̶l̶m̶e̶n̶t̶e̶ ̶e̶l̶ ̶s̶i̶s̶t̶e̶m̶a̶ ̶e̶s̶t̶a̶ ̶i̶m̶p̶l̶e̶m̶e̶n̶t̶a̶d̶o̶
̶s̶o̶b̶r̶e̶ ̶u̶n̶ ̶s̶e̶r̶v̶i̶d̶o̶r̶ ̶U̶D̶P̶,̶ ̶l̶o̶ ̶c̶u̶a̶l̶ ̶e̶n̶ ̶l̶a̶s̶ ̶e̶s̶p̶e̶c̶i̶f̶i̶c̶a̶c̶i̶o̶n̶e̶s̶ p̶e̶d̶i̶a̶
̶q̶u̶e̶ ̶s̶e̶a̶ ̶i̶m̶p̶l̶e̶m̶e̶n̶t̶a̶d̶o̶ ̶s̶o̶b̶r̶e̶ ̶u̶n̶ ̶s̶e̶r̶v̶i̶d̶o̶r̶ ̶T̶C̶P̶,̶ ̶e̶l̶ ̶s̶i̶s̶t̶e̶m̶a̶ ̶t̶i̶e̶n̶e̶
̶i̶m̶p̶l̶e̶m̶e̶n̶t̶a̶d̶o̶ ̶u̶n̶a̶ ̶s̶e̶r̶i̶e̶ ̶d̶e̶ ̶f̶u̶n̶c̶i̶o̶n̶a̶l̶i̶d̶a̶d̶e̶s̶,̶ ̶n̶o̶ ̶t̶o̶d̶a̶s̶̶a̶s̶ ̶q̶u̶e̶ ̶s̶e̶
̶p̶i̶d̶e̶n̶ ̶y̶ ̶l̶a̶s̶ ̶i̶m̶p̶l̶e̶m̶e̶n̶t̶a̶d̶a̶s̶ ̶n̶o̶ ̶r̶e̶s̶p̶e̶t̶a̶ ̶p̶a̶r̶a̶m̶e̶t̶r̶o̶s̶ ̶e̶x̶p̶u̶e̶s̶t̶o̶s̶ ̶e̶n̶
̶l̶o̶s̶ ̶r̶e̶q̶u̶e̶r̶i̶m̶i̶e̶n̶t̶o̶s̶.̶.
Update: El proyecto actualmente cumple con todos los requerimientos del trabajo

*Una recomendacion para empezar a modificar el sistema, crear una rama que estire de developer exponiendo en el nombre
de la rama con un verbo infinitvo que funcion se implementa en la rama.

*El Sistema respeta la estructura de archivos de graddle, el manejo de de dependencias se realiza tambien bajo graddle

*Las librerias utilizadas utilizadas

1. Postgresql42.2.2 (conexion a la base de datos)
2. Hibernate5.2.16.Final (ORM)
3. Gson2.8.2 (manejo de datos en formato json)

---

## Estructura de proyecto

El sistema respeta la estructura de archivos graddle
``````
banco_sd
│
├── main
│   ├── java
│   │   └── com
│   │       ├── api
│   │       │   └── statics
│   │       └── servidor
│   │           ├── app
│   │           ├── entity
│   │           └── statics
│   └── resources
└── test
    └── java
        └── com
            └── servidor
                └── util

``````
# Preparar entorno y clonar proyecto
Para que los scrips funcionen correctamente se debe crear una carpeta developer en home, el proyecto tiene que estar en ~/developer/
```
cd $home
mkdir developer
cd developer
sudo apt install git
git clone git clone https://gitlab.com/adrian.py93/grupo_08_t1.git
```

## Base de datos
El ORM Hibernate esta configurado para funcionar sobre postgresql9.5, se debera tener el motor de base de datos
y una base de datos creada con el nombre 'sd' con owner 'postgres' y pass 'postgres' La poblacion de la base de datos
se realiza al momento de correr los test

###instalacion de la base de datos
Ubuntu y Distros Debian based
```
sudo apt install postgresql postgresql-contrib pgadmin3
```
#####Crear y configurar base de datos:
hay que crear una base de datos de nombre 'sd' para el usuario 'postgres' con contraseña 'postgres'
```
sudo su postgres
psql
ALTER USER postgres with password 'postgres';
CREATE DATABASE sd WITH OWNER postgres;
\q
```

## Sevidor de aplicacion y cliente
Para correr el servidor de aplicacion hay que ejecutar  /main/java/com/servidor/app/server.java
``````
.
├── main
│   ├── java
│   │   └── com
│   │       ├── api
│   │       └── servidor
│   │           └── app
│   │               └── Server.java <------ !!RUN ME PLZ!!
│   └── resources
└── test
    └── java
        └── com
            └── servidor
                └── util
``````
El cliente se simula por medio de las pruebas unitarias, cada caso de uso es se realiza medio de una prueba unitaria
basta con correr los test 'CON EL SERVIDOR EN MARCHA' para y revisar las terminales del servidor y el cliente
para ver las interacciones.

``````
                                         ▄              ▄
.                                       ▌▒█           ▄▀▒▌
├── main                                ▌▒▒█        ▄▀▒▒▒▐
│   ├── java                           ▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
│   │   └── com                      ▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
│   │       ├── api                 ▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
│   │       │   └── statics        ▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌          
│   │       └── servidor           ▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐              
│   │           ├── app           ▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌                  
│   │           ├── entity        ▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌                    
│   │           └── statics      ▌▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐                    
└── test                         ▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌
    └── java                     ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐
        └── com                  ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐
            └── servidor              too much test wow                    
                ├── consultarSaldo.java     <------ !!RUN ME PLZ!!
                ├── depositarTest.java      <------ !!RUN ME PLZ!!
                ├── ExtraerTest.java        <------ !!RUN ME PLZ!!
                ├── JsonUsuarioTest.java    <------ !!RUN ME PLZ!!
                ├── transferirTest.java     <------ !!RUN ME PLZ!!
                └── util
``````